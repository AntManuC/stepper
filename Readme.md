# Sterownik silnika krokowego

## Cele do realizacji
- [x] Wykonanie prostego sterownika silnika krokowego bazującego na module Arduino Nano
- [ ] Regulacja prędkości obrotowej za pomocą enkodera.
- [x] Zmiana kierunku obrotów
- [x] Praca pół/pełno krokowa
- [x] Enable/Disable na pinie L297

## Użyte elementy
1. Bipolarny silnik krokowy.
2. L298 – moduł mostka
3. L297 - sterownik silnika krokowego
4. Arduino Nano z uC Atmega328P
5. Enkoder
6. Przyciski chwilowe
7. Elementy pasywne: kondensatory, rezystory, diody
8. zasilacz 12VDC
9. Płytka połączeniowa
10. PRzewody

## Elementy wymagające zakupu:
- [x] L298 – moduł mostka
- [x] Arduino Nano
- [x] L297 - sterownik silnika krokowego

## Środowisko programistyczne:
1. Atmel Studio
2. Programator AVR ISP mkII

# Wersja 1
## Działanie układu.
<b>W opisie użyto oznaczeń na płytce PCB Arduino.</b><p></p>
<b>Pierwsze uruchomienie.</b><br>
- silnik zmienia kierunek i rozdzielczość w sposób niekontrolowany. <br>
- przyczyna - przebiegi prostokątne na pinach A1 i A3 modułu Arduino.<br>
- usunięcie SN7414. Drgania zniknęły. W układzie eliminacji drgania styków pozostają tylko elementy RC.<br>
<p></p>


![](./Pics/9.jpg)
<p></p>
<p></p><br>

## Stan kodu:
1. Działa tylko timer generujący częstotliwość zegara dla L297.

## Kolejne etapy:
1. Podłączenie SW1/SW2 i enkodera do Arduino
2. Napisanie kodu do obsługi pozostałych peryferiów.
3. Testy.

# Wersja 2

## Działanie układu.
Dodano:
- sygnalizację stanu portu diodami LED.
- przyciśnięcie przycisku SW1 lub SW2 powoduje zmianę stanu na pinie A1 lub A3. <br>
- stan pinu pozostaje niezmienny do momentu kolejnego wciśnięcia wybranego przycisku. <p></p>

![](./Pics/11.jpg)

<p></p>
<p></p>

# Wersja 3

## Działanie układu.

Dodano:
- schemat w wersji 2.
- przycisk do sterowania pinem ENA/DISABLE w L297 (biała strzałka).
- sygnalizację stanu portu ENA/DISABLE. Czerwona dioda LED (niebieska strzałka).

Po włączeniu zasilania czerwona dioda LED jest włączona. L297 jest zablokowany.<br>
Nie świecą diody LED w module L298 (żółta strzałka). Wał silnika nie obraca się. <br>
Po naciśnięciu przycisku L297 zostaje odblokoway.<p></p>

![](./Pics/12.jpg)

<p></p>
<p></p>
<br>
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/RWqdqtzFHT0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Link do video.

Poniżej link przedstawiający działanie układu bez obsługi enkodera.

<a href="https://www.youtube.com/watch?v=RWqdqtzFHT0">VIDEO</a>









