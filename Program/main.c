/*
 * stepper.c
 *
 * 
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//-----------------------------dot przyciskow---------------
#define HALF_FULL PC0 // button switch connected to port PC0 pin 0
#define CW_CCW PC2 //przycisk podlaczony do PC2
#define ENABLE PC4 //przycisk podlaczony do PC4

#define ENC_LEFT PD2 // enkoder w lewo INT0
#define ENC_RIGHT PD3 // enkoder w prawo INT1

#define HALF_FULL297 PC1 // sterowanie z HALF_FULL297 z uC na L297
#define CW_CCW297 PC3 // sterowanie CW_CCW297 z uC na L297
#define ENABLE297 PC5 // sterowanie ENABLE297 z uC na L297

#define DEBOUNCE_TIME 25 
#define LOCK_INPUT_TIME 300





ISR(INT0_vect)
{
	PORTD &= ~(1<<PD0);  //test zapalania przy przelaczaniu diodda RX
	_delay_ms (150);
	PORTD |= (1<<PD0);
	
	//OCR1A--;
	
}

ISR(INT1_vect)
{
	PORTD &= ~(1<<PD1);	//test zapalania przy przelaczaniu dioda TX
	_delay_ms (150);
	PORTD |= (1<<PD1);
	
	//OCR1A++;
	
}


void init_ports_mcu()
{
	DDRC=0xFFu; 
	DDRC &= ~(1<<HALF_FULL);
	DDRC &= ~(1<<CW_CCW);
	DDRC &= ~(1<<ENABLE);
	PORTC = 0x0F;  
	
	EIMSK |= (1<<INT0);
	EICRA |= /*(1<<ISC00) |*/ (1<<ISC01);     //opadajace

	EIMSK |= (1<<INT1);
	EICRA |= /*(1<<ISC10) |*/  (1<<ISC10) | (1<<ISC11);     //oba zbocza

	DDRD |= (1<<PD2);
	PORTD |= (1<<PD2);
	DDRD |= (1<<PD3);
	PORTD |= (1<<PD3);
	
	DDRD |= (1<<PD0);
	PORTD |= (1<<PD0);
	DDRD |= (1<<PD1);
	PORTD |= (1<<PD1);
	
}
//----------------------------------------------------------


//------------------dot przyciskow--------------
unsigned char button_state()
{
	if (!(PINC & (1<<HALF_FULL))) return 1;
	return 0;
}

unsigned char button2_state()
{
	if (!(PINC & (1<<CW_CCW))) return 1;
	return 0;
}

unsigned char button3_state()
{
	if (!(PINC & (1<<ENABLE))) return 1;
	return 0;
}


static void SetTim()
{
	TCCR1B |= (1<<WGM12);
	TCCR1A |= (1<<COM1A0); // jeden kanal na PB1   
	TCCR1B |= (1<<CS12); // (1<<CS11) | (1<<CS10); (1<<CS12) ;
	OCR1A = 0x41;
	DDRB |= (1<<PB1);
	
}


int main (void) {
	sei();
	init_ports_mcu();
	
	//------------------przyciski-------------------
	unsigned char przycisk = 1; 
	unsigned char przycisk2 = 1;
	unsigned char przycisk3 = 1;
	
	//------------------przyciski------------
	

 SetTim();//0x01
 while(1){
    
  //-------------przyciski---------------------------
  if (button_state()) 
		{

			switch(przycisk){
				case 1:
				PORTC ^= (1<<HALF_FULL297); 
				przycisk=0; 
				break;
			}
			przycisk++; 
			_delay_ms(300);
		}
		
		//-----------------------------------------------------------------------------------------------------
		if (button2_state()) 
		{

			switch(przycisk2){
				case 1:
				PORTC ^= (1<<CW_CCW297); 
				przycisk2=0; 
				break;
			}
			przycisk2++; 
			_delay_ms(300);
		}
		
		if (button3_state()) 
				{

			switch(przycisk3){
				case 1:
				PORTC ^= (1<<ENABLE297); 
				przycisk3=0; 
				break;
			}
			przycisk3++; 
			_delay_ms(300);
		}
  //-------------przyciski------------------------
 }
}
